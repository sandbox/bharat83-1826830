<?php
module_load_include('inc', 'authnet_cim', 'authnet_cim.pages');
/** Set Configuration Form for
 * Authorize.net CIM
 **/
function authnetcim_validate_account($settings){
  global $user;
  variable_set('authnet_cim_apilogin',$settings['authnet_cim_apilogin']);
  variable_set('authnet_cim_transkey',$settings['authnet_cim_transkey']);
  variable_set('authnet_cim_use_sandbox',$settings['authnet_cim_use_sandbox']);
  variable_set('authnet_cim_transaction_mode',$settings['authnet_cim_transaction_mode']);
  variable_set('authnet_cim_use_taxable', $settings['authnet_cim_use_taxable']);
  variable_set('authnet_cim_trans_type', $settings['authnet_cim_trans_type']);
  /** check whether api user key and transaction key is fine **/
  $response=authnet_cim_create_profile($user->uid,$user->mail,$user->name);
  if(!$response)
        if(!$response->isOK()){
          if($response->xml->messages->message->code=='E00005' || $response->xml->messages->message->code=='E00006' || $response->xml->messages->message->code=='E00007')
            drupal_set_message($response->xml->messages->message->text,_cim_message_type(1));
        }
        else
        return true;
}
/** Set Configuration Form for
 * Authorize.net CIM
 **/
function authnet_cim_commerce_settings_form($settings = NULL) {
  //Define login and transaction key variables for Authenticate.Net
  if(isset($settings['authnet_cim_accountinfo'])){
      authnetcim_validate_account($settings['authnet_cim_accountinfo']);
  }
  $form = array();
 
  $form['authnet_cim_accountinfo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authorize.NET Account'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['authnet_cim_accountinfo']['authnet_cim_apilogin'] = array(
    '#type' => 'textfield',
    '#title' => 'API Login ID',
    '#description' => t('The login username used for the Authorize.net service.'),
    '#default_value' => variable_get('authnet_cim_apilogin', ''),
    '#required' => true,
  );

  $form['authnet_cim_accountinfo']['authnet_cim_transkey'] = array(
    '#type' => 'textfield',
    '#title' => 'Transaction Key',
    '#description' => t('Your API transaction key for sending encrypted data.'),
    '#default_value' => variable_get('authnet_cim_transkey', ''),
    '#required' => true,
  );
  $form['authnet_cim_accountinfo']['authnet_cim_transaction_mode'] = array(
    '#type' => 'select',
    '#title' => t('Transaction mode'),
    '#description' => t('"Production" and "Test Mode" will submit transactions using the login id and transaction key entered above.  "Production" sumbits real transactions.  "Test Mode" submits transactions to the URL for a developers test account, using the login information below.'),
    '#options' => array(
      'No' => t('Production'),
      'Yes' => t('Test Mode'),
    ),
    '#default_value' => variable_get('authnet_cim_transaction_mode', 'test'),
  );
  $form['authnet_cim_accountinfo']['authnet_cim_use_sandbox'] = array(
    '#type' => 'select',
    '#title' => t('Use Development Sandbox'),
    '#default_value' => variable_get('authnet_cim_use_sandbox', true),
    '#options' => array('yes' => 'Yes', 'no' => 'No'),
  );
  $form['authnet_cim_accountinfo']['authnet_cim_use_taxable'] = array(
    '#type' => 'select',
    '#title' => t('Taxable'),
    '#default_value' => variable_get('authnet_cim_use_taxable', true),
    '#options' => array('true' => 'Yes', 'false' => 'No'),
  );
  $form['authnet_cim_accountinfo']['authnet_cim_trans_type'] = array(
    '#type' => 'select',
    '#title' => t('Transaction Type'),
    '#default_value' => variable_get('authnet_cim_trans_type', 'AuthCapture'),
    '#options' => array('AuthOnly' => 'Authorization Only', 'AuthCapture' => 'Authorization and Capture','Refund'=>'Refund','CaptureOnly'=>'Capture Only','PriorAuthCapture'=>'Prior Authorization and Capture','Void'=>'Void'),
    '#description'=>t('Note The only transaction types that generate a customer receipt email are Authorization Only, Authorization and Capture, and Refund.'),
  );
  
  /*// Developer test mode settings
  $form['authnet_cim_settings']['test_mode_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('CIM test mode settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('These login settings are used instead of the above ones while in the test transaction mode. By setting the seperate login information below, you can save CIM credit card info to your developers account while testing.'),
  );
  $form['authnet_cim_settings']['test_mode_settings']['authnet_cim_login_id_TEST'] = array(
    '#type' => 'textfield',
    '#title' => t('Login ID'),
    '#default_value' => variable_get('cim_login_id_TEST', ''),
    '#description' => t('The login username used for the Authorize.net DEVELOPER TEST ACCOUNT.'),
  );
  $form['authnet_cim_settings']['test_mode_settings']['authnet_cim_transaction_key_TEST'] = array(
    '#type' => 'textfield',
    '#title' => t('Transaction key'),
    '#default_value' => variable_get('cim_transaction_key_TEST', ''),
    '#description' => t('Your API transaction key for sending encrypted data to your Authorize.net DEVELOPER TEST ACCOUNT.'),
  );*/
  // Checkout page settings
  /*$form['authnet_cim_checkout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Checkout page Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t(''),
  );
  $form['authnet_cim_checkout']['authnet_cim_form_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Checkout Form Title'),
    '#default_value' => variable_get('authnet_cim_form_title', 'CheckOut Form Using Authorize.Net CIM'),
    '#description' => t('Title For Checkout Form'),
  );
  $form['authnet_cim_checkout']['authnet_cim_form_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Checkout Form Title'),
    '#default_value' => variable_get('authnet_cim_form_title', 'CheckOut Form Using Authorize.Net CIM'),
    '#description' => t('Title For Checkout Form'),
  );*/
  //$settings=rules_config_load('authnet_cim_commerce');
  
  /*$form['submit']=array(
    '#type'=>'submit',
    '#value'=>'Save Configuration',
  );*/
  
  return $form;
}
/*function authnet_cim_commerce_settings_form_validate($form,&$form_state){
  ///** Set Authorize.Net CIM API constants 
  variable_set('authnet_cim_apilogin',$form_state['values']['authnet_cim_apilogin']);
  variable_set('authnet_cim_transkey',$form_state['values']['authnet_cim_transkey']);
  variable_set('authnet_cim_use_sandbox',$form_state['values']['authnet_cim_use_sandbox']);
  variable_set('authnet_cim_transaction_mode',$form_state['values']['authnet_cim_transaction_mode']);
  variable_set('authnet_cim_form_title',$form_state['values']['authnet_cim_form_title']);


drupal_set_message(t('The configuration options have been saved.'));
}

function authnet_cim_commerce_settings_form_submit($form,&$form_state){
 
  ///** Set Authorize.Net CIM API constants 
  variable_set('authnet_cim_apilogin',$form_state['values']['authnet_cim_apilogin']);
  variable_set('authnet_cim_transkey',$form_state['values']['authnet_cim_transkey']);
  variable_set('authnet_cim_use_sandbox',$form_state['values']['authnet_cim_use_sandbox']);
  variable_set('authnet_cim_transaction_mode',$form_state['values']['authnet_cim_transaction_mode']);
  variable_set('authnet_cim_form_title',$form_state['values']['authnet_cim_form_title']);


drupal_set_message(t('The configuration options have been saved.'));
}*/


/**  CIM payment form **/
function authnet_cim_commerce_payment_form() {
  // Merge default values into the default array.
  
  
global $user,$base_url;
$uid=$user->uid;
/** get details of existing credit card and shipping address for current user **/
if(authnet_cim_card_exist($uid)){
  $card = db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$uid,':pid'=>2));
  $options=array();
  
  $count=0;
    foreach($card as $key=>$customerPaymentProfileId){
      
      $count++;
            $customerPaymentProfileId=$customerPaymentProfileId->pvalue;
            $paymentprofile=authnet_cim_getpaymentprofile($customerPaymentProfileId);
            $options[$customerPaymentProfileId]=t('Use Card Ending with :number',array(':number'=>$paymentprofile->xml->paymentProfile->payment->creditCard->cardNumber));      
      
      
    }
          $form['credit_card']['card'] = array(
           '#type' => 'radios',
           '#title' => t(':totalcard Cards related to your account, ',array(':totalcard'=>$count)).l('Add a New Card',"$base_url/user/$user->uid/payment-profiles/create",array('query'=>drupal_get_destination())),
           '#options'=>$options, 
            );
          
          
          
          
}
else {
  
  $form['credit_card']['card'] = array(
           '#type' => 'item',
           '#markup' => t('No Card Found, ').l('Add a Card',"$base_url/user/$user->uid/payment-profiles/create",array('query'=>drupal_get_destination())),
            );
}
  return $form;
}


/** setting up checkout form for commerce CIM integration **/
/**
 * Payment method callback: checkout form.
 */
function authnet_cim_commerce_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  
  return authnet_cim_commerce_payment_form();
 
}


/**
 * Payment method callback: checkout form validation.
 */
function authnet_cim_commerce_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  
}

function cim_get_data($uid,$email,$description){
  /*foreach (commerce_checkout_pages() as $page_id => $checkout_page) {
        foreach (commerce_checkout_panes(array('page' => $page_id, 'enabled' => TRUE, 'review' => TRUE)) as $pane_id => $checkout_pane_local) {

        }
  }
  
   $panes = commerce_checkout_panes();
   
    echo "<pre>";
  print_r($panes);
  echo "</pre>";
  die; */
  /** GET customerProfileId ***/
    $customerProfileId=db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$uid,':pid'=>1))->fetchField();
    if($customerProfileId){
      $data['profileid']=$customerProfileId;
    }
    else{
      /** create profile **/
      $response=authnet_cim_create_profile($uid,$email,$description);
      $data['profileid']=$response->getCustomerProfileId();
    }
    
    /** GET paymentProfileId ***/
    $paymentProfileId=db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$uid,':pid'=>2))->fetchField();
    if($paymentProfileId){
      $data['paymentid']=$paymentProfileId;
    }
    else{
      /** create profile **/
      //$response=authnet_cim_create_profile($uid,$email,$description);
      //$data['profileid']=$response->getCustomerProfileId();
      
    }
    /** GET shippingProfileId ***/
    $shippingProfileId=db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$uid,':pid'=>3))->fetchField();
    if($shippingProfileId){
      $data['shippingid']=$shippingProfileId;
    }
    else{
      /** create profile **/
      //$response=authnet_cim_create_profile($uid,$email,$description);
      //$data['profileid']=$response->getCustomerProfileId();
      
    }
      //$paymentProfileId=db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$uid,':pid'=>2))->fetchField();
      //$customerAddressId=db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$uid,':pid'=>3))->fetchField();
      return $data;
}
/**
 * Payment method callback: checkout form submission.
 */
function authnet_cim_commerce_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
global $user;
  // Build a name-value pair array for this transaction.
  
  $trans = array(
    'trans_tax' => $payment_method['settings']['authnet_cim_accountinfo']['authnet_cim_use_taxable'],
    'trans_method' => 'CC',
    'trans_amount' => commerce_currency_amount_to_decimal($charge['amount'], $charge['currency_code']),
    'trans_card_num' => $pane_values['credit_card']['card'],
    //'trans_exp_date' => $pane_values['credit_card']['exp_month'] . $pane_values['credit_card']['exp_year'],
  );
  
  
  if($user->uid){
    $uid=$user->uid;
    $email=$user->mail;
    $description=$user->name;
    cim_get_data($uid,$email,$description);
  }
  else {}
  if(authnet_cim_profile_exist($uid)){
    
    $data=cim_get_data($user->uid,$user->mail,$user->name);
    ($pane_values['credit_card']['card'])?$customerPaymentProfileId=$pane_values['credit_card']['card']:authnetcim_create_paymentid($data['profileid'],$paymentdetails);
      $request = new AuthorizeNetCIM;
      
      // Create Auth & Capture Transaction
      $transaction = new AuthorizeNetTransaction;
      $transaction->amount = $trans['trans_amount'];
      $transaction->customerProfileId = $data['profileid'];
      $transaction->customerPaymentProfileId = $customerPaymentProfileId;
      $transaction->customerShippingAddressId = $data['shippingid'];
          
      $transaction->lineItems=array();
      $order_wrapper = entity_metadata_wrapper('commerce_order', $order);   // get all lineitems
      
      $lineItem              = new AuthorizeNetLineItem;
foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
  
      $lineItem=new stdClass;
      $line_item = $line_item_wrapper->value();
      
  /** add line items to the CIM payment **/
      $lineItem->itemId      = $line_item->line_item_id;
      $lineItem->name        = $line_item->line_item_label;
      $lineItem->description = variable_get('authnet_cim_trans_type', 'AuthCapture');
      $lineItem->quantity    = $line_item->quantity;
      $lineItem->unitPrice   = $line_item->commerce_unit_price['und']['0']['amount'];
      $lineItem->taxable     = $trans['trans_tax'];
 $transaction->lineItems[] = $lineItem;
 
}

      
      $response = $request->createCustomerProfileTransaction(variable_get('authnet_cim_trans_type', 'AuthCapture'), $transaction);
      
      $transactionResponse = $response->getTransactionResponse();
      $transactionId = $transactionResponse->transaction_id;
      $transactionmessage=$transactionResponse->xml->messages->message->text;
      drupal_set_message($transactionmessage);
  }
  
}


function authnetcim_tax_type($txn_type) {
  switch ($txn_type) {
    case COMMERCE_CREDIT_AUTH_ONLY:
      return 'AUTH_ONLY';
    case COMMERCE_CREDIT_PRIOR_AUTH_CAPTURE:
      return 'PRIOR_AUTH_CAPTURE';
    case COMMERCE_CREDIT_AUTH_CAPTURE:
      return 'AUTH_CAPTURE';
    case COMMERCE_CREDIT_REFERENCE_SET:
    case COMMERCE_CREDIT_REFERENCE_TXN:
    case COMMERCE_CREDIT_REFERENCE_REMOVE:
    case COMMERCE_CREDIT_REFERENCE_CREDIT:
      return NULL;
    case COMMERCE_CREDIT_CREDIT:
      return 'CREDIT';
    case COMMERCE_CREDIT_VOID:
      return 'VOID';
  }
}

function _cim_mode(){
  switch(variable_get('authnet_cim_transaction_mode','Yes')){
    case 'Yes':
      return 'testMode';
    break;
    case 'No':
      return 'liveMode';
    default:
      return 'testMode';
    break;
  }
}


