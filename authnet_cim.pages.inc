<?php
function authnet_cim_profile_page(){
    
    global $user;
    switch(arg(3)){
        case('create'):
            return drupal_get_form('authnet_cim_create_profile_form');
        break;
        case('change'):
            return drupal_get_form('authnet_cim_create_profile_form');
        break;
        case('shipping'):
            return drupal_get_form('authnet_cim_create_shipping_form');
        break;
        case('delete'):
            return drupal_get_form('authnet_cim_delete_confirmation');
        break;
        case('deleteshipping'):
            return drupal_get_form('authnet_cim_delete_confirmation');
        break;
        case('updateshipping'):
            return drupal_get_form('authnet_cim_create_shipping_form');
        break;
        default:
            authnet_cim_create_profile($user->uid,$user->mail,$user->name);
            return drupal_get_form('authnet_cim_profile_form');
        break;
    }
    
}

/** check profile exist in drupal database
 * @$uid= userid given by drupal
 *
 **/
function authnet_cim_profile_exist($uid){
    
    $flag = db_query("Select count(pid) from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$uid,':pid'=>1))->fetchField();
    return $flag;
}
/** Request CIM profile
 * @$merchantCustomerId = Customer id provided by drupal
**/
function authnet_cim_getprofile($merchantCustomerId){
    $customerProfileId = db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$merchantCustomerId,':pid'=>1))->fetchField();
    
    if($customerProfileId){
        /** Fetch CIM record from Authorize.net **/
        $customerProfile = new AuthorizeNetCIM;
        $response=$customerProfile->getCustomerProfile($customerProfileId);
        
    }
    else{
        $response= FALSE;
    }
    return $response;
}

/** Create customer Profile on Authorize.net CIM and Drupal both
 * @$merchantCustomerId= merchant customer id given by drupal
 * $email = email of customer
 * $description = description of customer
 *
 * return customerProfileid ;
 **/
function authnet_cim_create_profile($merchantCustomerId,$email,$description){
    global $base_url;
    $flag=authnet_cim_profile_exist($merchantCustomerId);
    $mode=variable_get('authnet_cim_transaction_mode','');
    if(!$flag){
        
        $request = new AuthorizeNetCIM;
        // Create new customer profile
        $customerProfile = new AuthorizeNetCustomer;
        $customerProfile->description = trim($description);
        $customerProfile->merchantCustomerId= check_plain(trim($merchantCustomerId));
        $customerProfile->email = trim($email);
        $response = $request->createCustomerProfile($customerProfile);
        
        //echo "<pre>";print_r($response);die;
        if ($response->isOk())
        {
            $customerProfileId = $response->getCustomerProfileId();
            /** Entry to drupal database **/
            
            authnet_cim_insert_data($merchantCustomerId,1,$customerProfileId,$email);
            $arg_array=array(':category'=>'Customer',':id'=>$customerProfileId,'@userlink'=>"$base_url/user/$merchantCustomerId",':user'=>$description);
            $watchdog=_make_watchdog($arg_array,'created');
            //_make_watchdog($arg_array,'created');
            
            //if test mode show drupal message
            $message=t(':category Profile :id , Created for <a href="@userlink">:user</a> on Authorize.net CIM',$arg_array);
                
        }
        else {
            //if test mode show drupal message
            $message=t('Error :errorcode, :errortext',array(':errorcode'=>$response->xml->messages->message->code,':errortext'=>$response->xml->messages->message->text));
        }
        if($mode) drupal_set_message($message);
    }
    else
    {
        $response=$flag;
        //if test mode show drupal message
            //$message=t('(TESTMODE) Authorize.net CIM ');
    }
            //if test mode show drupal message
            
            
        //echo $message;die($mode);
  return $response;
}

/** Check card info exist
 * @$uid= userid given by drupal
 * 
 **/
function authnet_cim_card_exist($uid){
    $flag = db_query("Select count(pid) from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$uid,':pid'=>2))->fetchField();
    return $flag;
}
/** Check card info exist for a given userid
 * @$uid= userid given by drupal
 * 
 **/
function authnet_cim_shippingadd_exist($uid){
    $flag = db_query("Select count(pid) from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$uid,':pid'=>3))->fetchField();
    return $flag;
}

function authnet_cim_profile_form($form, &$form_state){
    global $user;
    $count=0;
    /** Billing and Credit Card info Starts **/
    $form['authnet_cim_card_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Card Information'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    );
     /** check any card information exist **/
    if(!authnet_cim_card_exist($user->uid)){
        
        $form['authnet_cim_card_info']['info'] = array(
        '#type' => 'item',
        '#title' => t('No Card Information'),
        '#markup' => l('Add Card Information','user/'.$user->uid.'/payment-profiles/create'),
        );
    }
    else{
        /** get payment profile ids **/
        $result=db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$user->uid,':pid'=>2));
        /*echo "<pre>";
        print_r($profile);
        echo "</pre>";*/
        $form['authnet_cim_card_info']['newcard'] = array(
            '#type' => 'item',
            '#markup' => l('Add New Card','user/'.$user->uid.'/payment-profiles/create'),
            );
        foreach($result as $key=>$customerPaymentProfileId){
            $count++;
            $customerPaymentProfileId=$customerPaymentProfileId->pvalue;
            $paymentprofile=authnet_cim_getpaymentprofile($customerPaymentProfileId);
            
            $customertype=$paymentprofile->xml->paymentProfile->customerType;
            $form['authnet_cim_card_info']['card'.$count] = array(
           '#type' => 'fieldset',
            '#title' => t('Card :number',array(':number'=>$paymentprofile->xml->paymentProfile->payment->creditCard->cardNumber)),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
            );
            $form['authnet_cim_card_info']['card'.$count]['info'] = array(
            '#type' => 'item',
            '#markup' => l('edit','user/'.$user->uid.'/payment-profiles/change',array('query' => array('id' => $customerPaymentProfileId))).' '.l('delete','user/'.$user->uid.'/payment-profiles/delete',array('query' => array('id' => $customerPaymentProfileId))),
            );
            /** Show all fields with there values for billing information and credit card information**/
            $form['authnet_cim_card_info']['card'.$count]['customerType'] = array(
                '#type' => 'item',
                '#title' => t('Customer Type'),
                '#markup' => '<div class="markup">'.$customertype.'</div>',
            );
            foreach($paymentprofile->xml->paymentProfile->billTo[0] as $key=>$value){
              $form['authnet_cim_card_info']['card'.$count][$key] = array(
                '#type' => 'item',
                '#title' => t($key),
                '#markup' => '<div class="markup">'.$value.'</div>',
            );
            }
            
            foreach($paymentprofile->xml->paymentProfile->payment->creditCard[0] as $key=>$value){
              $form['authnet_cim_card_info']['card'.$count][$key] = array(
                '#type' => 'item',
                '#title' => t($key),
                '#markup' => '<div class="markup">'.$value.'</div>',
            );
            }
        }
        
    }
    
    /** Billing and Credit Card info End **/
    
  
  /** Shipping info Starts **/
  $form['authnet_cim_shipping_details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shipping Information'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    );
  /** check any shipping information exist**/
    if(!authnet_cim_shippingadd_exist($user->uid)){
        
        $form['authnet_cim_shipping_details']['info'] = array(
        '#type' => 'item',
        '#title' => t('No Shipping Information'),
        '#markup' => l('Add Shipping Information','user/'.$user->uid.'/payment-profiles/shipping'),
        );
    }
    else{
        $count=0;
        $result=db_query("Select pvalue from {authnet_cim_data} where uid=$user->uid and pid=3",array(':uid'=>$user->uid,':pid'=>3));
        
        $form['authnet_cim_shipping_details']['newshipping'] = array(
            '#type' => 'item',
            '#markup' => l('Add New Shipping Address','user/'.$user->uid.'/payment-profiles/shipping'),
            );
        foreach($result as $key=>$customerAddressId){
            $count++;
            $customerAddressId=$customerAddressId->pvalue;
            $shippingaddress=authnet_cim_getshippingprofile($customerAddressId);
            $form['authnet_cim_shipping_details']['address'.$count] = array(
           '#type' => 'fieldset',
            '#title' => t('Address :address',array(':address'=>$shippingaddress->xml->address->address)),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
            );
            $form['authnet_cim_shipping_details']['address'.$count]['info'] = array(
            '#type' => 'item',
            //'#title' => t('No Card Information'),
            '#markup' => l('edit','user/'.$user->uid.'/payment-profiles/updateshipping',array('query' => array('id' => $customerAddressId))).' '.l('delete','user/'.$user->uid.'/payment-profiles/deleteshipping',array('query' => array('id' => $customerAddressId))),
            );
            foreach($shippingaddress->xml->address[0] as $key=>$value){
              $form['authnet_cim_shipping_details']['address'.$count][$key] = array(
                '#type' => 'item',
                '#title' => t($key),
                '#markup' => '<div class="markup">'.$value.'</div>',
            );  
            }
        }
    }
    
    /** Shipping info Ends **/
  return $form;
}


/** Create payment information form **/ 
function authnet_cim_create_profile_form($form, &$form_state){
     global $user;
    if(arg(3)=='change'){
        $paymentid=$_GET['id'];
        //echo $paymentid;
       
        //$profile=authnet_cim_getprofile($user->uid);
        $paymentprofile=authnet_cim_getpaymentprofile($paymentid);
        $paymentprofile=$paymentprofile->xml;
        $customerType=$paymentprofile->paymentProfile->customerType;
        
        
        /** Show all fields with there values for billing information and credit card information**/
        
        foreach($paymentprofile->paymentProfile->billTo[0] as $key=>$value){
            $$key=$value;
          
        }
        foreach($paymentprofile->paymentProfile->payment->creditCard[0] as $key=>$value){
            $$key=$value;
        }
    }
    
    $form['authnet_cim_card_info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Add Card Details'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        );
    $form['authnet_cim_card_info']['customerType'] = array(
        '#type' => 'select',
        '#title' => 'Customer Type',
        '#options'=>authnet_customer_type(),
        '#required' => TRUE,
        '#default_value'=>(isset($customerType))?$customerType:'', 
      );
     
        
    $form['authnet_cim_card_info']['firstName'] = array(
        '#type' => 'textfield',
        '#title' => 'First Name',
        '#default_value'=>(isset($firstName))?$firstName:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_card_info']['lastName'] = array(
        '#type' => 'textfield',
        '#title' => 'Last Name',
        '#default_value'=>(isset($lastName))?$lastName:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_card_info']['company'] = array(
        '#type' => 'textfield',
        '#title' => 'Company',
        '#default_value'=>(isset($company ))?$company:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_card_info']['address'] = array(
        '#type' => 'textfield',
        '#title' => 'Address',
        '#default_value'=>(isset($address))?$address:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_card_info']['city'] = array(
        '#type' => 'textfield',
        '#title' => 'City',
        '#default_value'=>(isset($city))?$city:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_card_info']['state'] = array(
        '#type' => 'textfield',
        '#title' => 'State/Province',
        '#default_value'=>(isset($state))?$state:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_card_info']['zip'] = array(
        '#type' => 'textfield',
        '#title' => 'Zip/Postal Code',
        '#default_value'=>(isset($zip))?$zip:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_card_info']['country'] = array(
        '#type' => 'textfield',
        '#title' => 'Country',
        '#default_value'=>(isset($country))?$country:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_card_info']['phoneNumber'] = array(
        '#type' => 'textfield',
        '#title' => 'Phone',
        '#default_value'=>(isset($phoneNumber))?$phoneNumber:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_card_info']['faxNumber'] = array(
        '#type' => 'textfield',
        '#title' => 'Fax',
        '#default_value'=>(isset($faxNumber))?$faxNumber:'', 
        //'#size' => 20,
        //'#required' => TRUE,
      );
    /*$form['authnet_cim_card_info']['shipping'] = array(
        '#type' => 'checkbox',
        '#title' => 'Create a Shipping Profile from the information above',
        //'#size' => 20,
        //'#required' => TRUE,
      );*/
      $form['authnet_cim_card_info']['cardNumber'] = array(
        '#type' => 'textfield',
        '#title' => 'Credit Card Number',
        '#size' => 20,
        '#maxlength' => 16,
        '#default_value'=>(isset($cardNumber))?$cardNumber:'',
        '#required' => TRUE,
      );
      $form['authnet_cim_card_info']['cc_expiration_year'] = array(
        '#type' => 'select',
        '#title' => 'Expiration Year',
        '#options' => drupal_map_assoc(range(date('Y'), date('Y') + 20)), 
        '#required' => TRUE,
      );
      $form['authnet_cim_card_info']['cc_expiration_month'] = array(
        '#type' => 'select',
        '#title' => 'Expiration Month',
        '#options' => authnet_card_months(), 
        '#required' => TRUE,
      );
      $form['authnet_cim_card_info']['cardCode'] = array(
        '#type' => 'textfield',
        '#title' => 'Security Code',
        '#size' => 5,
        '#maxlength' => 4,
        '#required' => TRUE,
      );
      $form['authnet_cim_card_info']['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Save',
        
      );
      
      $form['authnet_cim_card_info']['cancel'] = array(
	'#type' => 'button',
	'#attributes' => array('onClick' => 'location.replace("'.$_SERVER['HTTP_REFERER'].'"); return false;'),
	'#value' => t('Cancel'),
        );
      $form['authnet_cim_card_info']['returl'] = array(
        '#type'=>'hidden',
        '#value'=>$_GET['destination'],
      );
      return $form;
}

function authnet_cim_create_profile_form_validate($form,&$form_state)
{
    global $user;
    $card_number=$form_state['values']['cardNumber'];
    /*if(strlen($card_number)!=16)
    form_set_error($form_state['values']['cardNumber'],'Credit Card number should be 16 digits');*/
    
    
    /** check user exist on authorize.net or not
     * if not create user profile on authorize.net
     **/
    $merchantCustomerId=$user->uid;
    $email=$user->mail;
    $description=$user->name;
    $response=authnet_cim_create_profile($merchantCustomerId,$email,$description);
    if(!$response)
    form_set_error('','Some problem, Authorize.Net is not responding');
}

function authnet_cim_create_profile_form_submit($form,&$form_state)
{
    global $user,$base_url;
    /** set return url **/
    
   
    ($form_state['values']['returl'])?$returl=$form_state['values']['returl']:$returl=url("$base_url/user/$user->uid/payment-profiles");
    $card_number=$form_state['values']['cardNumber'];
    $expiry_date=$form_state['values']['cc_expiration_year'].'-'.$form_state['values']['cc_expiration_month'];
    $customerProfileId='';
    
        // Add payment profile.
    $request = new AuthorizeNetCIM;
    $paymentProfile = new AuthorizeNetPaymentProfile;
    foreach($form_state['values'] as $key=>$value){
        if($key=='cc_expiration_month');
        else if($key=='cc_expiration_year');
        else if($key=='customerType');
        //else if($key=='shipping'){($value)?$addshippingflag='true':$addshippingflag='false';}
        else if($key=='cardCode' ||$key=='cardNumber')  $paymentProfile->payment->creditCard->$key=$value;
        else if($key=='submit') break;
        else $paymentProfile->billTo->$key=$value;
    }
    $paymentProfile->customerType = $form_state['values']['customerType'];
    $paymentProfile->payment->creditCard->cardNumber = $card_number;
    $paymentProfile->payment->creditCard->expirationDate = $expiry_date;
    // if update request
    if(arg(3)=='change'){ 
        $paymentProfileId=$_GET['id'];
        /** check $paymentProfileId exist or not **/
        $uid=db_query("Select uid from {authnet_cim_data} where pvalue=:pvalue",array(':pvalue'=>$paymentProfileId))->fetchField();
        if($uid==$user->uid) {
            $customerProfileId=authnet_cim_getcustomerid($paymentProfileId);
            $response = $request->updateCustomerPaymentProfile($customerProfileId,$paymentProfileId, $paymentProfile);
            if($response->isOk()) {
                $message=t('Customer Payment Profile Updated Successfully');
                $watchdog=_make_watchdog(array(':category'=>'Payment',':id'=>$paymentProfileId,'@userlink'=>"user/$user->uid",':user'=>$user->name),'updated');
            }
            else {
                $message=t(':errortext',array(':errortext'=>$response->xml->messages->message->text));
                $watchdog=_make_watchdog(array(':category'=>'Payment','@userlink'=>"user/$user->uid",':user'=>$user->name,':errorcode'=>$response->xml->messages->message->code,':errortext'=>$response->xml->messages->message->text),'error');
            }
        }
        else {
            $message =t('Id not exist in Database');
            $watchdog=t('Error : Payment Profile Id not exist in Database for <a href="@userlink">:user</a>',array('@userlink'=>"user/$user->uid",':user'=>$user->name));
        }
    }
    // create customer profile request
    else {
        
        $customerProfileId=db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$user->uid,':pid'=>1))->fetchField();
        if($customerProfileId) {
            
            $response = $request->createCustomerPaymentProfile($customerProfileId,$paymentProfile,_cim_mode());
            if($response->isOk()) {
                $paymentProfileId=$response->xml->customerPaymentProfileId;
                    authnet_cim_insert_data($user->uid,2,$paymentProfileId,$user->mail);
                    $message=t('Customer Payment Profile Created Successfully');
                    $type=_cim_message_type(0);
                    $watchdog=_make_watchdog(array(':category'=>'Payment',':id'=>$paymentProfileId,'@userlink'=>"user/$user->uid",':user'=>$user->name),'created');
                    
                }
            else {
                $message=t(':errortext Please fill the form again.',array(':errortext'=>$response->xml->messages->message->text));
                $type=_cim_message_type(2);
                $watchdog=_make_watchdog(array(':category'=>'Payment','@userlink'=>"user/$user->uid",':user'=>$user->name,':errorcode'=>$response->xml->messages->message->code,':errortext'=>$response->xml->messages->message->text),'error');
                $returl=url("$base_url/user/$user->uid/payment-profiles/create");
            }
        }
        else{
            $message=t('Customer Profile is not exist on Authorize.net CIM');
            $type=_cim_message_type(1);
            $watchdog=t('Customer Profile Not Exist for <a href="@userlink">:user</a>. ',array('@userlink'=>"user/$user->uid",':user'=>$user->name));
        }
        
            
    }    
    watchdog("authnet_cim",$watchdog);
    drupal_set_message($message,$type);
    drupal_goto($returl);
}


/** Create Shipping information form **/ 
function authnet_cim_create_shipping_form($form, &$form_state){
    if(arg(3)=='updateshipping'){
        $shippingid=$_GET['id'];
        global $user;
        $shippingprofile=authnet_cim_getshippingprofile($shippingid);
        
        /** Show all fields with there values for Shipping information**/
        
        foreach($shippingprofile->xml->address[0] as $key=>$value){
            $$key=$value;
          
        }
        
    }
    
    $form['authnet_cim_ship_info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Add Card Details'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        );
    $form['authnet_cim_ship_info']['firstName'] = array(
        '#type' => 'textfield',
        '#title' => 'First Name',
        '#default_value'=>(isset($firstName))?$firstName:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_ship_info']['lastName'] = array(
        '#type' => 'textfield',
        '#title' => 'Last Name',
        '#default_value'=>(isset($lastName))?$lastName:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_ship_info']['company'] = array(
        '#type' => 'textfield',
        '#title' => 'Company',
        '#default_value'=>(isset($company))?$company:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_ship_info']['address'] = array(
        '#type' => 'textfield',
        '#title' => 'Address',
        '#default_value'=>(isset($address))?$address:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_ship_info']['city'] = array(
        '#type' => 'textfield',
        '#title' => 'City',
        '#default_value'=>(isset($city))?$city:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_ship_info']['state'] = array(
        '#type' => 'textfield',
        '#title' => 'State/Province',
        '#default_value'=>(isset($state))?$state:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_ship_info']['zip'] = array(
        '#type' => 'textfield',
        '#title' => 'Zip/Postal Code',
        '#default_value'=>(isset($zip))?$zip:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_ship_info']['country'] = array(
        '#type' => 'textfield',
        '#title' => 'Country',
        '#default_value'=>(isset($country))?$country:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_ship_info']['phoneNumber'] = array(
        '#type' => 'textfield',
        '#title' => 'Phone',
        '#default_value'=>(isset($phoneNumber))?$phoneNumber:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
    $form['authnet_cim_ship_info']['faxNumber'] = array(
        '#type' => 'textfield',
        '#title' => 'Fax',
        '#default_value'=>(isset($faxNumber))?$faxNumber:''
        //'#size' => 20,
        //'#required' => TRUE,
      );
      $form['authnet_cim_ship_info']['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Save',
        
      );
      
      $form['authnet_cim_ship_info']['cancel'] = array(
	'#type' => 'button',
	'#attributes' => array('onClick' => 'location.replace("'.$_SERVER['HTTP_REFERER'].'"); return false;'),
	'#value' => t('Cancel'),
        );
      return $form;
}

function authnet_cim_create_shipping_form_validate($form,&$form_state)
{
    foreach($form_state['values'] as $key=>$value){
        
    }
}
//shipping submit comes here

function authnet_cim_create_shipping_form_submit($form,&$form_state)
{
    global $user;
    $profile=authnet_cim_getprofile($user->uid);
    //print_r($profile);
    $customerProfileId = $profile->getCustomerProfileId();
     //$shipping_id=$profile->getCustomerAddressId();
     
    // Add shipping address.
    $request = new AuthorizeNetCIM;
    $address = new AuthorizeNetAddress;
    foreach($form_state['values'] as $key=>$value){
       if($key=='submit') break;
       else   $address->$key = $value;
    }

    if(arg(3)=='updateshipping'){
        $shipping_id=$_GET['id'];
        $uid=db_query("Select uid from {authnet_cim_data} where pvalue=:pvalue",array(':pvalue'=>$shipping_id))->fetchField();
        if($uid==$user->uid) {
            $customerProfileId=authnet_cim_getcustomerid($shipping_id);
            $response = $request->updateCustomerShippingAddress($customerProfileId, $shipping_id, $address);
            if($response->isOk()) {
                $message=t('Customer Shipping Profile Updated Successfully');
                $watchdog=_make_watchdog(array(':category'=>'Shipping',':id'=>$shipping_id,'@userlink'=>"user/$user->uid",':user'=>$user->name),'updated');
            }
            else {
                $message=t('some error');
                $watchdog=_make_watchdog(array(':category'=>'Shipping','@userlink'=>"user/$user->uid",':user'=>$user->name,':errorcode'=>$response->xml->messages->message->code,':errortext'=>$response->xml->messages->message->text),'error');
            }
        }
        else {
            $message =t('Id not exist in Database');
            $watchdog=t('Error : Shipping Profile Id not exist in Database for <a href="@userlink">:user</a>',array('@userlink'=>"user/$user->uid",':user'=>$user->name));
        }
        //$shipping_id = $profile->getCustomerAddressId();
        //$response = $request->updateCustomerShippingAddress($customerProfileId, $shipping_id, $address);
    }
    else{
      $customerProfileId=db_query("Select pvalue from {authnet_cim_data} where uid=:uid and pid=:pid",array(':uid'=>$user->uid,':pid'=>1))->fetchField();
        if($customerProfileId) {
            $response = $request->createCustomerShippingAddress($customerProfileId, $address);
            if($response->isOk()) {
                $shipping_id=$response->xml->customerAddressId;
                    authnet_cim_insert_data($user->uid,3,$shipping_id,$user->mail);
                    $message=t('Customer Shipping Profile Created Successfully');
                    $watchdog=_make_watchdog(array(':category'=>'Shipping',':id'=>$shipping_id,'@userlink'=>"user/$user->uid",':user'=>$user->name),'created');
                }
            else {
                $message=t('some error');
                $watchdog=_make_watchdog(array(':category'=>'Shipping','@userlink'=>"user/$user->uid",':user'=>$user->name,':errorcode'=>$response->xml->messages->message->code,':errortext'=>$response->xml->messages->message->text),'error');
            }
        }
        else{
            $message=t('Customer Profile is not exist on Authorize.net CIM');
            $watchdog=t('Customer Profile Not Exist for <a href="@userlink">:user</a>. ',array('@userlink'=>"user/$user->uid",':user'=>$user->name));
        }
        
            
    }    
    watchdog("authnet_cim",$watchdog);
    drupal_set_message($message);
    drupal_goto("user/$user->uid/payment-profiles");
    }
    



/** delete profile and shipping information
 * depending on the url
 **/
function authnet_cim_delete_confirmation(){
    $id=$_GET['id'];
    $form['authnet_cim_delete_confirmation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Are you sure you want to delete :type ?',array(':type'=>$id)),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );
    $form['authnet_cim_delete_confirmation']['info'] = array(
        '#type' => 'item',
        '#markup' => t('This action cannot be undone.'),
        );
    $form['authnet_cim_delete_confirmation']['confirm'] = array(
        '#type' => 'submit',
        '#value' => 'Confirm',    
      );
    $form['authnet_cim_delete_confirmation']['cancel'] = array(
	'#type' => 'button',
	'#attributes' => array('onClick' => 'location.replace("'.$_SERVER['HTTP_REFERER'].'"); return false;'),
	'#value' => t('Cancel'),
        );
      return $form;
    
}

function authnet_cim_delete_confirmation_submit(){
    global $user;
    $deleteid=$_GET['id'];
    $message = authnet_cim_delete_data($deleteid);
    if($message)
        drupal_set_message($message);
    else
        drupal_set_message('Id not found in Database');
    drupal_goto("user/$user->uid/payment-profiles");
}

/** Delete data depending on type
 * @id= id get by query string

**/
function authnet_cim_delete_data($pvalue){
    global $user;
    $profiletype=db_query("Select pid,uid from {authnet_cim_data} where pvalue=:pvalue",array(':pvalue'=>$pvalue))->fetchAssoc();
    if($profiletype['pid'] && $profiletype['uid']==$user->uid){
    $type=$profiletype['pid'];
    $customerProfileId=authnet_cim_getcustomerid($pvalue);
    $request = new AuthorizeNetCIM;
        
        switch($type){
            case '1':  // Customer profile
                $response = $request->deleteCustomerProfile($customerProfileId);
                // define variable for the db update
                $dbfield='Customer Profile';
            break;
            case '2': // payment profile
                
                $response = $request->deleteCustomerPaymentProfile($customerProfileId, $pvalue);
                // define variable for the db update
                $dbfield='Card Information';
                
            break;
            case '3': // Shipping Address
                
                $response = $request->deleteCustomerShippingAddress($customerProfileId, $pvalue);
                // define variable for the db update
                $dbfield='Shipping Address';
                
            break;
        }
        
        if($response->isOk()){
            db_delete('authnet_cim_data') 
             ->condition('pvalue', $pvalue)
             ->execute();
            watchdog("authnet_cim", ':pvalue : :type Deleted by <a href="@userlink">:user</a>',array(':pvalue'=>$pvalue,':type'=>$dbfield,'@userlink'=>"user/$user->uid",':user'=>$user->name,':errorcode'=>$response->xml->messages->message->code,':errortext'=>$response->xml->messages->message->text));
            return $response->xml->messages->message->text;
        }
        else{
            watchdog("authnet_cim", ':pvalue : Error in Deletion of :type by <a href="@userlink">:user</a>, Error : :errorcode, :errortext',array(':pvalue'=>$pvalue,':type'=>$dbfield,'@userlink'=>"user/$user->uid",':user'=>$user->name,':errorcode'=>$response->xml->messages->message->code,':errortext'=>$response->xml->messages->message->text));
            return $response->xml->messages->message->text;
        }
    }
    else return FALSE;
}
/** insert  authnet_cim_data table **/
function authnet_cim_insert_data($merchantCustomerId,$pid,$pvalue,$email){
    db_insert('authnet_cim_data')
                    ->fields(array(
                      'uid' => $merchantCustomerId,
                      'pid' => $pid,
                      'pvalue' => $pvalue,
                      'created' => REQUEST_TIME,
                      'changed' => REQUEST_TIME,
                      'email'=>$email,
                ))
                ->execute();
}


/** helper function to set drupal message type **/
function _cim_message_type($type){
    switch($type){
        case 0:
            return 'status';
        break;
        case 1:
            return 'warning';
        break;
        case 2:
            return 'error';
        break;
    }
}
